package Zadanie2;

import lombok.Getter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
public class MailServer {
    private List<Client> connectedClients = new ArrayList<>();

    public void connect(Client... clients){
       Collections.addAll(connectedClients, clients);
    }

    public void sendMessage(Mail mail, Client sender){
        for (Client c : connectedClients) {
            if(!c.equals(sender)){
                mail.setNadawca(sender.getName());
                mail.setOdbiorca(c.getName());
                mail.setDataNadania(LocalDate.now());
                c.readMail(mail);
            }
        }
    }
    public void printConnections(){
        System.out.println("Connected clients:");
        for (Client c : connectedClients) {
            System.out.println(c);
        }
    }
}
