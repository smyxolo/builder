package Zadanie2;

import java.time.LocalDate;

import static Zadanie2.MessageType.*;

public class MailFactory {
    private Mail.Builder template = new Mail.Builder()
            .setNadawca("Tomasz")
            .setOdbiorca("Michał")
            .setAdresIPNadania(234)
            .setAdresIPOdbioru(424)
            .setCzySzyfrowane(true)
            .setIsSpam(false)
            .setSerwerPosredni("3948.23498.239")
            .setDataNadania(LocalDate.now())
            .setProtokolKomunikacji("MyProtocol")
            .setSkrzynkaPocztowa("Onet");

    public Mail createUnknown(String tresc){
        return template
                .setMessageType(UNKNOWN)
                .setTresc(tresc)
                .createMail();
    }

    public Mail createOffer(String tresc){
        return template
                .setMessageType(OFFER)
                .setTresc(tresc)
                .createMail();
    }

    public Mail createSocial(String tresc){
        return template
                .setMessageType(SOCIAL)
                .setTresc(tresc)
                .createMail();
    }

    public Mail createNotification(String tresc){
        return template
                .setMessageType(NOTIFICATIONS)
                .setTresc(tresc)
                .createMail();
    }

    public Mail createForum(String tresc){
        return template
                .setMessageType(FORUM)
                .setTresc(tresc)
                .createMail();
    }
}
