package Zadanie2;

public class Main {
    public static void main(String[] args) {
        MailServer m = new MailServer();
        Client c1 = new Client("Tomasz");
        Client c2 = new Client("Marek");
        Client c3 = new Client("Aleksander");
        m.connect(c1, c2, c3);
        m.printConnections();
        System.out.println();

        MailFactory mailFactory = new MailFactory();
        m.sendMessage(mailFactory.createForum("Pojawił się nowy wpis w twoim wątku."), c1);
        m.sendMessage(mailFactory.createNotification("Check your sms inbox!"), c2);
        m.sendMessage(mailFactory.createOffer("SamsungGEARs za piątala"), c3);
        m.sendMessage(mailFactory.createSocial("Grzegorz Brzęczyszczykiewicz dał ci lajka"), c1);
        m.sendMessage(mailFactory.createUnknown("Ale o co chodzi?"), c2);

        System.out.println();

        System.out.println(c1.getName() + ": " + c1.getSkrzynka() + "\n");
        System.out.println(c2.getName() + ": " + c2.getSkrzynka()+ "\n");
        System.out.println(c3.getName() + ": " + c3.getSkrzynka()+ "\n");
    }
}
