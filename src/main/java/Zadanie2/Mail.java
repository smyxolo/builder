package Zadanie2;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter

public class Mail {
    private String nadawca;
    private String odbiorca;
    private String tresc;
    private LocalDate dataNadania;
    private LocalDate dataOdbioru;
    private int adresIPNadania;
    private int adresIPOdbioru;
    private String serwerPosredni;
    private String skrzynkaPocztowa;
    private String protokolKomunikacji;
    private MessageType messageType;
    private boolean czySzyfrowane;
    private boolean isSpam;

    public Mail(String nadawca, String odbiorca, String tresc, LocalDate dataNadania, LocalDate dataOdbioru, int adresIPNadania, int adresIPOdbioru, String serwerPosredni, String skrzynkaPocztowa, String protokolKomunikacji, MessageType messageType, boolean czySzyfrowane, boolean isSpam) {
        this.nadawca = nadawca;
        this.odbiorca = odbiorca;
        this.tresc = tresc;
        this.dataNadania = dataNadania;
        this.dataOdbioru = dataOdbioru;
        this.adresIPNadania = adresIPNadania;
        this.adresIPOdbioru = adresIPOdbioru;
        this.serwerPosredni = serwerPosredni;
        this.skrzynkaPocztowa = skrzynkaPocztowa;
        this.protokolKomunikacji = protokolKomunikacji;
        this.messageType = messageType;
        this.czySzyfrowane = czySzyfrowane;
        this.isSpam = isSpam;
    }

    public static class Builder{

        private String nadawca;
        private String odbiorca;
        private String tresc;
        private LocalDate dataNadania;
        private LocalDate dataOdbioru;
        private int adresIPNadania;
        private int adresIPOdbioru;
        private String serwerPosredni;
        private String skrzynkaPocztowa;
        private String protokolKomunikacji;
        private MessageType messageType;
        private boolean czySzyfrowane;
        private boolean isSpam;

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setOdbiorca(String odbiorca) {
            this.odbiorca = odbiorca;
            return this;
        }

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setDataNadania(LocalDate dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setDataOdbioru(LocalDate dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Builder setAdresIPNadania(int adresIPNadania) {
            this.adresIPNadania = adresIPNadania;
            return this;
        }

        public Builder setAdresIPOdbioru(int adresIPOdbioru) {
            this.adresIPOdbioru = adresIPOdbioru;
            return this;
        }

        public Builder setSerwerPosredni(String serwerPosredni) {
            this.serwerPosredni = serwerPosredni;
            return this;
        }

        public Builder setSkrzynkaPocztowa(String skrzynkaPocztowa) {
            this.skrzynkaPocztowa = skrzynkaPocztowa;
            return this;
        }

        public Builder setProtokolKomunikacji(String protokolKomunikacji) {
            this.protokolKomunikacji = protokolKomunikacji;
            return this;
        }

        public Builder setMessageType(MessageType messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder setCzySzyfrowane(boolean czySzyfrowane) {
            this.czySzyfrowane = czySzyfrowane;
            return this;
        }

        public Builder setIsSpam(boolean isSpam) {
            this.isSpam = isSpam;
            return this;
        }

        public Mail createMail() {
            return new Mail(nadawca, odbiorca, tresc, dataNadania, dataOdbioru, adresIPNadania, adresIPOdbioru, serwerPosredni, skrzynkaPocztowa, protokolKomunikacji, messageType, czySzyfrowane, isSpam);
        }
    }

    @Override
    public String toString() {
        return "\n{" +
                "Od: " + nadawca +
                ": '" + tresc + '\'' +
                "-> type: " + messageType +
                '}';
    }
}
