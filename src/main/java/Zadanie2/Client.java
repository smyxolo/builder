package Zadanie2;

import lombok.Getter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Client {
    private String name;
    private List<Mail> skrzynka = new ArrayList<>();

    public Client(String name) {
        this.name = name;
    }

    public void readMail(Mail mail){
        mail.setDataOdbioru(LocalDate.now());
        mail.setOdbiorca(this.name);
        skrzynka.add(mail);
        System.out.println("Klient " + name + " otrzymal maila.");
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                '}';
    }
}
