 Builder:
- Dzięki  niemu  nie  musimy  pisać  konstruktorów  N  parametrowych.
- Pozwala  na t worzenie  obiektów  w  których  możemy  w  kolejności  ustawiać
parametry.
- Nie  musimy t worzyć  kombinacji  konstruktorów  dla  parametrów  które  są
opcjonalne
- Można  go  wygenerować
https://bitbucket.org/nordeagda2/designpatternbuilderexample
Zadanie  1: Stworzymy  aplikację  symulującą  komunikację  między  użytkownikami.  Aplikacja  będzie działać  na  zasadzie  pokoju  chat'owego.
Stwórz  klasę  Mail  która  posiada  pola: - t resc
-  nadawca
-  data  nadania
-  data  odbioru
-  adres i p  nadania
-  adres i p  odebrania
-  nazwe  serwera  posredniego
-  nazwe  skrzynki  pocztowej
-  protokol  komunikacji - t yp  wiadomości ( UNKNOWN,  OFFER,  SOCIAL,  NOTIFICATIONS,  FORUM) - f lagę(boolean) -   czy  szyfrowane
- f lagę(boolean) -  i sSpam
Stwórz  klasę  Client  która: -  posiada  pole  name
-  posiada  pole  List<Mail> -  l ista/skrzynka  wiadomości  klienta
- r eadMail(Mail  m) -   która  powoduje  dodanie  wiadomości  do  skrzynki i  wypisanie komunikatu:
"Klient "   + t his.getName()  + "   otrzymal  maila"
Stwórz  klasę  MailServer  która  posiada l istę  podłączonych  klientów ( pole  klasy).  Metody: -  connect(Client  c) -   powoduje  podłączenie  klienta  do  serwera,  czyli  dodanie  go  do
listy (  t ej  która j est  polem  klasy)
-  disconnect(Client  c) -   powoduje  odłączenie  klienta -  sendMessage(Mail  m,  Client  sender) -   powoduje r ozesłanie  wiadomości  do
wszystkich  klientów  oprócz  nadawcy.  Przed  przekazaniem  wiadomości  należy  ustawić j ej datę  odebrania  na t eraz ( now()).
Sprawdź  poprawność  działania  aplikacji.   Napisz t esty  weryfikujące  poprawność  działania aplikacji.  DLA  CHĘTNYCH
 
 TREŚĆ  DODATKOWA: Zakładamy  że i stnieją  pewne t ypy  maili.  Np.  na  skrzynce  pocztowej  gmail  maile  są segregowane  na  skrzynki:  społecznościowe,  powiadomienia,  oferty, f ora.
Dodatkowo  do  poprzedniej t reści  stwórz f abrykę  wiadomości  która  dla  wszystkich t ypów wiadomości  pozwala  na  szybkie  utworzenie  wiadomości  przesyłając  do f abryki j edynie t reść.
podpowiedź:  stwórz f abrykę,  gdzie  kazda  z  metod  posiada j eden  parametr i j est  nim t reść maila,  a r esztę  obiektu  stwórz  korzystając  ze  wcześniej  stworzonego  buildera.
Wykorzystaj  metody f abryki  w  mainie ( stwórz  maile) i  prześlij j e  wykorzystując  klienta. Zweryfikuj  poprawność  działania  aplikacji,  dopisz t esty.
https://bitbucket.org/nordeagda2/designpatternmail
 