package Zadanie3Pieczątka;

public class Main {
    public static void main(String[] args) {
        Stamp s = new Stamp.Builder()
                .setFirstDayNumber(1)
                .setSecondDayNumber(3)
                .setSecondMonthNumber(5)
                .setThirdYearNumber(1)
                .setFourthYearNumber(7)
                .setCaseNumber(20)
                .createStamp();

        System.out.println(s);
    }
}
