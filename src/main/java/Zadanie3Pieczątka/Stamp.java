package Zadanie3Pieczątka;

public class Stamp {
    private int firstDayNumber = 0;
    private int secondDayNumber = 1;
    private int firstMonthNumber = 0;
    private int secondMonthNumber = 1;
    private int firstYearNumber = 2;
    private int secondYearNumber = 0;
    private int thirdYearNumber = 0;
    private int fourthYearNumber = 1;
    private int caseNumber = 001;

    public Stamp(int firstDayNumber, int secondDayNumber, int firstMonthNumber, int secondMonthNumber, int firstYearNumber, int secondYearNumber, int thirdYearNumber, int fourthYearNumber, int caseNumber) {
        this.firstDayNumber = firstDayNumber;
        this.secondDayNumber = secondDayNumber;
        this.firstMonthNumber = firstMonthNumber;
        this.secondMonthNumber = secondMonthNumber;
        this.firstYearNumber = firstYearNumber;
        this.secondYearNumber = secondYearNumber;
        this.thirdYearNumber = thirdYearNumber;
        this.fourthYearNumber = fourthYearNumber;
        this.caseNumber = caseNumber;
    }

    public static class Builder {
        private int firstDayNumber = 0;
        private int secondDayNumber = 1;
        private int firstMonthNumber = 0;
        private int secondMonthNumber = 1;
        private int firstYearNumber = 2;
        private int secondYearNumber = 0;
        private int thirdYearNumber = 0;
        private int fourthYearNumber = 1;
        private int caseNumber = 001;

        public Builder setFirstDayNumber(int firstDayNumber) {
            this.firstDayNumber = firstDayNumber;
            return this;
        }

        public Builder setSecondDayNumber(int secondDayNumber) {
            this.secondDayNumber = secondDayNumber;
            return this;
        }

        public Builder setFirstMonthNumber(int firstMonthNumber) {
            this.firstMonthNumber = firstMonthNumber;
            return this;
        }

        public Builder setSecondMonthNumber(int secondMonthNumber) {
            this.secondMonthNumber = secondMonthNumber;
            return this;
        }

        public Builder setFirstYearNumber(int firstYearNumber) {
            this.firstYearNumber = firstYearNumber;
            return this;
        }

        public Builder setSecondYearNumber(int secondYearNumber) {
            this.secondYearNumber = secondYearNumber;
            return this;
        }

        public Builder setThirdYearNumber(int thirdYearNumber) {
            this.thirdYearNumber = thirdYearNumber;
            return this;
        }

        public Builder setFourthYearNumber(int fourthYearNumber) {
            this.fourthYearNumber = fourthYearNumber;
            return this;
        }

        public Builder setCaseNumber(int caseNumber) {
            this.caseNumber = caseNumber;
            return this;
        }

        public Stamp createStamp() {
            return new Stamp(firstDayNumber, secondDayNumber, firstMonthNumber, secondMonthNumber, firstYearNumber, secondYearNumber, thirdYearNumber, fourthYearNumber, caseNumber);
        }
    }

    @Override
    public String toString() {
        return  firstDayNumber + "" + secondDayNumber +
                "/" + firstMonthNumber + secondMonthNumber +
                "/" + firstYearNumber  + secondYearNumber + thirdYearNumber + fourthYearNumber +
                ": " + caseNumber;
    }
}
