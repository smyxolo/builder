package Zadanie1;

//Stwórz aplikację a w niej klasę GameCharacter która jest postacią w grze. Postać powinna mieć pola:
//        name,
//        health,
//        mana,
//        number of points
//        Stwórz w niej buildera.
//        W mainie stwórz kolekcję postaci (listę lub set) i dodaj do niej kilka takich samych postaci, oraz
//        kilka postaci odrobine zmodyfikowanych (punktami lub mana i zyciem).

public class GameCharacter {
    private String name;
    private int health;
    private int mana;
    private int numberOfPoints;

    public GameCharacter(String name, int health, int mana, int numberOfPoints) {
        this.name = name;
        this.health = health;
        this.mana = mana;
        this.numberOfPoints = numberOfPoints;
    }

    public static class Builder{

        private String name;
        private int health;
        private int mana;
        private int numberOfPoints;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setNumberOfPoints(int numberOfPoints) {
            this.numberOfPoints = numberOfPoints;
            return this;
        }

        public GameCharacter createGameCharacter(){
            return new GameCharacter(name, health, mana, numberOfPoints);
        }
    }

    @Override
    public String toString() {
        return "\n{" +
                "name: " + name +
                ", health:" + health +
                ", mana:" + mana +
                ", points:" + numberOfPoints +
                "}";
    }
}
