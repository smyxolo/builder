package Zadanie1;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<GameCharacter> gameCharacterSet = new HashSet<>();
        GameCharacter.Builder gm = new GameCharacter.Builder()
                .setHealth(30)
                .setMana(80)
                .setName("Zbyszek")
                .setNumberOfPoints(98);

        gameCharacterSet.add(gm.createGameCharacter());
        gameCharacterSet.add(gm.createGameCharacter());
        gameCharacterSet.add(gm.createGameCharacter());
        gameCharacterSet.add(gm.setHealth(24).setMana(42).setName("Olek").setNumberOfPoints(187).createGameCharacter());
        gameCharacterSet.add(gm.setName("Jarosław").setMana(0).createGameCharacter());
        gameCharacterSet.add(gm.setName("Jerzy").createGameCharacter());

        System.out.println(gameCharacterSet);
    }
}
