package Zadanie2;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.time.LocalDate;
import static org.hamcrest.core.IsEqual.equalTo;

public class Zadanie2Tests {
    private MailServer m;
    private Client c1;
    private Client c2;
    private Client c3;

    @Before
    public void resetValues() {
        m = new MailServer();
        c1 = new Client("Tomasz");
        c2 = new Client("Marek");
        c3 = new Client("Ania");

    }

    @Test
    public void checkConnections(){
        m.connect(c1, c2, c3);
        Assert.assertEquals(3, m.getConnectedClients().size());
    }


    @Test
    public void sendMail(){
        m.connect(c1, c2, c3);
        Mail mail = new Mail.Builder()
                .setAdresIPNadania(02734)
                .setIsSpam(false)
                .setNadawca(c1.getName())
                .createMail();
        m.sendMessage(mail, c1);
        Assert.assertThat(mail, equalTo(c2.getSkrzynka().get(c2.getSkrzynka().size()-1)));
        Assert.assertThat(mail, equalTo(c3.getSkrzynka().get(c3.getSkrzynka().size()-1)));
    }

    @Test
    public void checkReceivingDate(){
        m.connect(c1, c2, c3);
        m.sendMessage(new Mail.Builder()
                .setAdresIPNadania(2342)
                .setIsSpam(true)
                .setNadawca(c2.getName())
                .createMail(), c2);

        Assert.assertEquals(LocalDate.now(), c3.getSkrzynka().get(c3.getSkrzynka().size()-1).getDataOdbioru());
    }

    @Test
    public void checkSendingDate(){
        m.connect(c1, c2, c3);
        m.sendMessage(new Mail.Builder()
                .setAdresIPNadania(234)
                .setIsSpam(false)
                .setNadawca(c2.getName())
                .createMail(), c2);

        Assert.assertEquals(LocalDate.now(), c1.getSkrzynka().get(c1.getSkrzynka().size()-1).getDataNadania());
    }



}
